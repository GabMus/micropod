use gettextrs::gettext;
use mpris::{PlaybackStatus, Player, PlayerFinder};

#[derive(Debug)]
pub struct MicroPodPlayer {
    player_finder: PlayerFinder,
}

impl MicroPodPlayer {
    pub fn new() -> Self {
        let player_finder = PlayerFinder::new().expect("Could not connect to D-Bus");
        Self { player_finder }
    }

    pub fn get_player(&self) -> Option<Player> {
        match self.player_finder.find_active() {
            Ok(p) => Some(p),
            Err(_) => match self.player_finder.find_first() {
                Ok(p) => Some(p),
                Err(_) => None,
            },
        }
    }

    pub fn get_name(&self) -> String {
        return match self.get_player() {
            Some(p) => p
                .get_metadata()
                .unwrap()
                .title()
                .unwrap_or(gettext("Unknown Title").as_str())
                .to_string(),
            None => "".to_string(),
        };
    }

    pub fn get_artist(&self) -> String {
        return match self.get_player() {
            Some(p) => p
                .get_metadata()
                .unwrap()
                .artists()
                .unwrap_or_default()
                .join(", ")
                .to_string(),
            None => "".to_string(),
        };
    }

    pub fn get_cover_art(&self) -> String {
        return match self.get_player() {
            Some(p) => p
                .get_metadata()
                .unwrap()
                .art_url()
                .unwrap_or_default()
                .to_string()
                .replace("file://", ""),
            None => "".to_string(),
        };
    }

    pub fn get_playback_status_icon(&self) -> &str {
        return match self.get_player() {
            Some(p) => match p.get_playback_status().unwrap() {
                PlaybackStatus::Playing => "media-playback-pause-symbolic",
                PlaybackStatus::Paused | PlaybackStatus::Stopped => "media-playback-start-symbolic",
            },
            None => "media-playback-start-symbolic",
        };
    }

    pub fn play_pause(&self) {
        match self.get_player() {
            Some(p) => {
                p.play_pause().ok();
                ()
            }
            None => (),
        }
    }

    pub fn skip_next(&self) {
        match self.get_player() {
            Some(p) => {
                p.next().ok();
                ()
            }
            None => (),
        };
    }

    pub fn skip_prev(&self) {
        match self.get_player() {
            Some(p) => {
                p.previous().ok();
                ()
            }
            None => (),
        };
    }
}
